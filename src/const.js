const data = [
  {
    id: 1,
    title: "Task Manager",
    to: "/task-manager",
    img: "red.png",
  },
  {
    id: 2,
    title: "Корзина",
    to: "/trash",
    img: "blue.svg",
  },
];

export { data };
