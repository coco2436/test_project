import { createRouter, createWebHistory } from "vue-router";

import TheHomePage from "@views/TheHomePage.vue";
import TheTaskManager from "@views/TheTaskManager.vue";
import TheTrash from "@views/TheTrash.vue";

const routes = [
  { path: "/", component: TheHomePage },
  {
    path: "/task-manager",
    component: TheTaskManager,
  },

  {
    path: "/trash",
    component: TheTrash,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
