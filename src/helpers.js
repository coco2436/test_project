const toHome = (router) => {
  router.push({ path: "/" });
};

export { toHome };
