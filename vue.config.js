const path = require("path");

const coreNodeModules = path.resolve(__dirname, "./node_modules");
const src = path.resolve(__dirname, "./src");
const assets = path.resolve(__dirname, "./src/assets");
const components = path.resolve(__dirname, "./src/components");
const views = path.resolve(__dirname, "./src/views");

module.exports = {
  chainWebpack: (config) => {
    config.plugins.delete("prefetch"),
      config.module
        .rule("mjs$")
        .test(/\.mjs$/)
        .include.add(/node_modules/)
        .end()
        .type("javascript/auto");

    return config;
  },
  configureWebpack: {
    resolve: {
      extensions: [".mjs", ".js", ".jsx", ".vue", ".json", ".wasm", ".*"],
      symlinks: false,
      modules: [coreNodeModules],
      alias: {
        vue$: "vue/dist/vue.esm-bundler.js",
        vue: path.resolve(__dirname, `./node_modules/vue`),
        "@": src,
        "@assets": assets,
        "@components": components,
        "@views": views,
      },
    },
  },
};
